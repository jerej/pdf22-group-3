import speech_recognition as sr
from pydub import AudioSegment
from pydub.playback import play
import RPi.GPIO as GPIO
import time

servoPIN = 23
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN, GPIO.OUT)

p = GPIO.PWM(servoPIN, 50) # GPIO 17 for PWM with 50Hz
p.start(2.5) # Initialization

recognizer = sr.Recognizer()

while True:

    try:

        with sr.Microphone() as mic:

            p.ChangeDutyCycle(0)
            recognizer.adjust_for_ambient_noise(mic, duration=0.1)
            recognizer.dynamic_energy_adjustment_ratio = 20
            audio = recognizer.listen(mic)

            f = open('testi.wav', 'wb')
            f.write(audio.get_wav_data())
            f.close()

            song = AudioSegment.from_wav("testi.wav")

            print("Aloittaa signaalin lähettämisen")
            p.ChangeDutyCycle(10)
            play(song)
            print("Lopettaa signaalin lähettämisen")
            p.ChangeDutyCycle(5)
            time.sleep(0.5)


    except KeyboardInterrupt:
        p.stop()
        GPIO.cleanup()
        break

    except sr.UnknownValueError():

        recognizer = sr.Recognizer()
        continue
    